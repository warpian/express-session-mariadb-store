A express session store with the mariadb connector


## Install

```sh
npm install express-session-mariadb-store
yarn add express-session-mariadb-store
```

make sure you have a database with a table create like the following example:

```sql
CREATE TABLE session(
  sid                     VARCHAR(100) PRIMARY KEY NOT NULL,   
  session                 VARCHAR(2048) DEFAULT '{}',   
  lastSeen                DATETIME DEFAULT NOW() 
);
```

## Use

commonJS

```js
const session = require('express-session')
const MariaDBStore = require('express-session-mariadb-store')

app.use(session({
  store: new MariaDBStore({
    user: 'user',
    password: 'password'
  })
}))
```

## Config
All config options for the MariaDBStore:

| key             | default     |
| :-------------- | :---------- |
| sessionTable    | 'session'   |
| host            | 'localhost' |
| user            | undefined   |
| password        | undefined   |
| database        | 'sessiondb' |
| connectionLimit | 5           |

