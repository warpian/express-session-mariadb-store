const mariadb = require('mariadb')
const session = require('express-session')

const noop = () => {}

module.exports = class MariaDBStore extends session.Store {

  constructor(options = {}) {
    super(options)
    if(options.user && options.password) {
      this.sessionTable = options.sessionTable || 'session'

      this.pool = mariadb.createPool({
        host: options.host || 'localhost',
        user: options.user,
        password: options.password,
        database: options.database || 'sessiondb',
        connectionLimit: options.connectionLimit || 5
      })
    } else {
      throw new Error('Must give user and password for database')
    }
  }

  get(sid, callback = noop) {
    this.query(`SELECT * FROM ${this.sessionTable} WHERE sid = ?`, [sid])
    .then(result => {
      if(result.length > 0) {
        callback(null, JSON.parse(result[0].session))
      } else {
        callback(null, null)
      }
    })
    .catch(err => callback(err))
  }

  set(sid, session, callback = noop) {
    this.query(`SELECT sid FROM ${this.sessionTable} WHERE sid = ?`, [sid])
    .then(result => {
      if(result.length > 0) {
        return this.query(`UPDATE ${this.sessionTable} SET session = ?, lastSeen = NOW() WHERE sid = ?`, [JSON.stringify(session), sid])
      } else {
        return this.query(`INSERT INTO ${this.sessionTable} (sid, session) VALUES (?, ?)`, [sid, JSON.stringify(session)])
      }
    })
    .then(() => callback())
    .catch(err => callback(err))
  }

  destroy(sid, callback = noop) {
    this.query(`DELETE FROM ${this.sessionTable} WHERE sid = ?`, [sid])
    .then(() => callback())
    .catch(err => callback(err))
  }

  clear(callback = noop) {
    this.query(`DELETE * FROM ${this.sessionTable}`)
    .then(() => callback())
    .catch(err => callback(err))
  }

  all(callback = noop) {
    this.query(`SELECT * FROM ${this.sessionTable}`)
    .then(result => callback(null, result))
    .catch(err => callback(err))
  }

  length(callback = noop) {
    this.query(`SELECT count(*) AS count FROM ${this.sessionTable}`)
    .then(result => callback(null, result[0].count))
    .catch(err => callback(err))
  }

  touch(sid, session, callback = noop) {
    this.set(sid, session, callback)
  }

  query(query, data) {
    if(this.pool) {
      return this.pool.getConnection()
        .then(conn => {
      
          return conn.query(query, data)
            .then(result => {
              conn.end();
              return result    
            })
            .catch(err => {
              //handle error
              conn.end();
              throw err
            })
          
      }).catch(err => {
        throw err
      })
    } else {
      throw new Error('No Mariadb Pool')
    }
  }
}